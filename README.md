#Comandos básicos - requisitos

##criar ambiente virtual com python
python -m venv venv

##ativar ambiente virtual (Linux, MacOS)
source ./venv/bin/activate

##ativar ambiente virtual (Windows)
venv\Scripts\activate

##instalar dependências
pip install -r requirements.txt

##rodar
python run.py