import os

basedir = os.path.abspath(os.path.dirname(__file__))

# Credenciais
user = 'root'
password = ''
host = '127.0.0.1'
database = 'teste'

class Config:
    SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{user}:{password}@{host}/{database}"