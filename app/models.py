from app import db
from datetime import datetime, timedelta

class Usuarios(db.Model):
    id_usuario = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False, unique=True)
    pontuacao = db.Column(db.Integer, nullable=False, default=0)
    data_nascimento = db.Column(db.Date, nullable=True)
    senha = db.Column(db.String(200), nullable=True)

class Tarefa(db.Model):
    id_tarefa = db.Column(db.Integer, primary_key=True)
    pontuacao_tarefa = db.Column(db.Integer, nullable=False, default=0)
    descricao_tarefa = db.Column(db.String(100), nullable=False)
    data_criacao = db.Column(db.Date, nullable=False, default=datetime.now)
    data_entrega = db.Column(db.String(100), nullable=False, default=lambda: datetime.now() + timedelta(days=2))

class Amizade(db.Model):
    id_amizade = db.Column(db.Integer, primary_key=True)
    id_amigo1 = db.Column(db.Integer, nullable=False)
    id_amigo2 = db.Column(db.Integer, nullable=False)