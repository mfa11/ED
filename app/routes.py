from flask import request, redirect, url_for, render_template
from app import app, db
from app.models import Usuarios, Tarefa, Amizade
import feedparser
from datetime import datetime

ano_atual = datetime.now().year

@app.route("/")
def home():
    return render_template("Cyberconsciência.html", ano_atual=ano_atual)

@app.route("/filmes")
def filmes():
    return render_template("filmesnet.html", ano_atual=ano_atual)

@app.route("/filmes/idiocracia")
def idiocracia():
    return render_template("idiocracia.html")

@app.route("/filmes/o_dilema_das_redes")
def odilemadasredes():
    return render_template("odilemadasredes.html")

@app.route("/filmes/o_mundo_depois_de_nos")
def omundodepoisdenos():
    return render_template("omundodepoisdenós.html")

@app.route("/filmes/nao_olhe_para_cima")
def naoolheparacima():
    return render_template("nãoolheparacima.html")

@app.route("/termos")
def termos():
    return render_template("termosdeuso.html", ano_atual=ano_atual)

@app.route("/objetivo")
def objetivo():
    return render_template("objetivodoprojetocc.html", ano_atual=ano_atual)

@app.route("/contato")
def contato():
    return render_template("contato2ed.html", ano_atual=ano_atual)

@app.route("/perfil")
def conta():
    feed = feedparser.parse('https://rss.nytimes.com/services/xml/rss/nyt/World.xml')
    news_entries = []

    for entry in feed.entries:
        image_url = entry.media_content[0]['url'] if 'media_content' in entry and entry.media_content else None
        news_entries.append({
            'title': entry.title,
            'link': entry.link,
            'description': entry.description,
            'image': image_url
        })

    return render_template("perfil.html", news_entries=news_entries)

@app.route("/perfil/info1")
def info1():
    return render_template("dopamina.html", ano_atual=ano_atual)

@app.route("/perfil/info2")
def info2():
    return render_template("segurança_digital.html", ano_atual=ano_atual)

@app.route("/perfil/info3")
def info3():
    return render_template("consequencias.html", ano_atual=ano_atual)

@app.route("/servico")
def servico():
    return render_template("serviços.html", ano_atual=ano_atual)

@app.route("/colaborar")
def colaborar():
    return render_template("comocolaborarcc.html", ano_atual=ano_atual)

@app.route("/atividades")
def atividade():
    return render_template("novosa.html", ano_atual=ano_atual)

@app.route("/atividades/atividade_fisica")
def atividadefisica():
    return render_template("edfis.html", ano_atual=ano_atual)
    matérias.html

@app.route("/atividades/disciplinas")
def materia():
    return render_template("matérias.html", ano_atual=ano_atual)

@app.route("/pontuacao")
def pontuacao():
    return render_template("rotinadispute.html")

@app.route("/login")
def login():
    return render_template("formulario.html")

@app.route("/listadeamigos")
def listadeamigos():
    return render_template("listadeamigos.html")